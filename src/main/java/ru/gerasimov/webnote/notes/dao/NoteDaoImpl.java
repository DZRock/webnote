/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.gerasimov.webnote.notes.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.gerasimov.webnote.notes.model.Note;

/**
 *
 * @author Admin
 */
@Repository
public class NoteDaoImpl implements NoteDao{

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void addNote(Note note) {
        sessionFactory.getCurrentSession().save(note);
    }

    @Override
    public List<Note> getAllNotes() {
        return sessionFactory.getCurrentSession().createQuery("select note from Note note").list();
    }

    @Override
    public void deleteNote(int id) {
        Note note = this.getNoteById(id);
        if(note != null)
            sessionFactory.getCurrentSession().delete(note);
    }

    @Override
    public Note getNoteById(int id) {
        return (Note) sessionFactory.getCurrentSession().createQuery("select note from Note note where id=?").setParameter(0,id).list().get(0);
    }

}
