/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.gerasimov.webnote.notes.dao;

import java.util.List;
import ru.gerasimov.webnote.notes.model.Note;

/**
 *
 * @author Admin
 */
public interface NoteDao {
    
    public void addNote(Note note);
    public List<Note> getAllNotes();
    public void deleteNote(int id);
    public Note getNoteById(int id);
}
