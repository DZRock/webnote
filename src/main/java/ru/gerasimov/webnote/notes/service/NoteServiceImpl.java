/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.gerasimov.webnote.notes.service;

import java.util.List;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gerasimov.webnote.notes.dao.NoteDao;
import ru.gerasimov.webnote.notes.model.Note;

/**
 *
 * @author Admin
 */
@Service
public class NoteServiceImpl implements NoteService{

    @Autowired
    private NoteDao noteDao;
    
    @Transactional
    @Override
    public void addNote(Note note) {
        noteDao.addNote(note);
    }

    @Transactional
    @Override
    public List<Note> getAllNotes() {
        return noteDao.getAllNotes();
    }

    @Transactional
    @Override
    public void deleteNote(int id) {
        noteDao.deleteNote(id);
    }

}
