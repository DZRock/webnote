/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.gerasimov.webnote.notes.service;

import java.util.List;
import org.springframework.stereotype.Service;
import ru.gerasimov.webnote.notes.model.Note;

/**
 *
 * @author Admin
 */
public interface NoteService {
    
    public void addNote(Note note);
    public List<Note> getAllNotes();
    public void deleteNote(int id);
}
