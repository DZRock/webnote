/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.gerasimov.webnote.web;

import java.util.Date;
import static javax.swing.text.StyleConstants.ModelAttribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.gerasimov.webnote.notes.model.Note;
import ru.gerasimov.webnote.notes.service.NoteService;

/**
 *
 * @author Admin
 */
@Controller
public class MainController {
    
    @Autowired
    NoteService noteService;
    
    @ModelAttribute
    public Note getNoteClass(){return new Note();}
    
    @RequestMapping(value = "/noteRequest",method = RequestMethod.GET)
    public ModelAndView noteList(){
        ModelAndView model = new ModelAndView();
        model.setViewName("notesList");
        model.addObject("notes",noteService.getAllNotes());
        return model;
    }
    
    @RequestMapping(value = "/noteRequest",method = RequestMethod.POST)
    public ModelAndView addNote(@ModelAttribute("note")Note note){
        ModelAndView model = new ModelAndView();
        model.setViewName("notesList");
        
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        note.setDate(sdf.format(new Date()));        
        noteService.addNote(note);
        
        model.addObject("notes",noteService.getAllNotes());
        return model;
    }

    @RequestMapping(value = "/deleteNote",method = RequestMethod.GET)
    public ModelAndView deleteNote(@RequestParam("id")int id){
        ModelAndView model = new ModelAndView();
        model.setViewName("notesList");

        noteService.deleteNote(id);

        model.addObject("notes",noteService.getAllNotes());
        return model;
    }
    
}
