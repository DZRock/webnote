<%-- 
    Document   : index
    Created on : 02.03.2015, 15:47:16
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:url value="/noteRequest" var="path"/>
        <form:form action="${path}" method="get">
            <input type="submit" value="go">
        </form:form>
    </body>
</html>
