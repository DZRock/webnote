<%-- 
    Document   : notesList
    Created on : 02.03.2015, 15:12:11
    Author     : Admin
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Web Note</title>
    </head>
    <body>
        <h1>Create new note</h1>
        <c:url value='/noteRequest.htm' var="path"/>
        
        <form:form action="${path}" modelAttribute="note" method="post">
            <p>
                <form:label path="title">Название заметки</form:label>
                <form:input path="title"></form:input>
            </p>
            <p>
                <form:label path="description">Опсиание</form:label>
                <form:textarea path="description" rows="5" cols="30"/>
            </p>
            <p>
                <input type="submit" value="Добавить"/>
            </p>
        </form:form>
        
        <table>
            <tr>
                    <td>Id</td>
                    <td>Title</td>
                    <td>Description</td>
                    <td>Date</td>
                </tr>
            <c:forEach items="${notes}" var="note">
                <tr>
                    <td>${note.id}</td>
                    <td>${note.title}</td>
                    <td>${note.description}</td>
                    <td>${note.date}</td>

                    <td>
                        <c:url value='/deleteNote' var="delete"/>
                        <form:form action="${delete}" method="get">
                            <input type="hidden" name="id" value="${note.id}"/>
                            <input type="submit" value="Удалить"/>
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
            
    </body>
</html>
